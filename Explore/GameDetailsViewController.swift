//
//  GameDetailsViewController.swift
//  Explore
//
//  Created by Rich Tanner on 10/23/17.
//  Copyright © 2017 Rich Tanner. All rights reserved.
//

import UIKit

class GameDetailsViewController: UIViewController {
    
    @IBOutlet weak var gameImageView: UIImageView!
    @IBOutlet weak var gameTitleLabel: UILabel!
    @IBOutlet weak var gameDescriptionLabel: UILabel!
    @IBOutlet weak var gameYearLabel: UILabel!
    @IBOutlet weak var gamePlayersLabel: UILabel!
    @IBOutlet weak var gamePublisherLabel: UILabel!
    @IBOutlet weak var gamePlatformLabel: UILabel!
    
    var thisGame: GameObject? // the GameObject we get passed in by the TableView

    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        // TODO: We should have been passed ONE copy of a GameObject, so HERE we can set our view outlets to those properties
        
        hookupOurModel()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        hookupOurModel()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        
        hookupOurModel()
    }
    
    @IBAction func dismissModalSelf(sender: AnyObject) {
        // un-set our game data before going away
        thisGame = nil
        self.presentingViewController?.dismiss(animated: true, completion: nil)
    }
    
    func hookupOurModel() {
        
        // when we have "var thisGame: GameObject?" up above, the question mark makes it an "Optional".  The pointer 'thisGame' is allocated either way, and th question mark means "maybe it's of this type"
        // the "if let" below forces the object to be unwrapped and set to the specified type
        
        if let unwrappedGame = thisGame {
            let localGameImage = UIImage(named: unwrappedGame.gameImageName)
            gameImageView.image = localGameImage
            
            gameTitleLabel.text = unwrappedGame.gameTitle
            gameDescriptionLabel.text = unwrappedGame.gameDescription
            gameYearLabel.text = String(unwrappedGame.gameReleaseYear)
            if unwrappedGame.minPlayers != unwrappedGame.maxPlayers {
                gamePlayersLabel.text = String(unwrappedGame.minPlayers) + " to " + String(unwrappedGame.maxPlayers) + " players"
            } else {
                gamePlayersLabel.text = String(unwrappedGame.minPlayers) + " player"
            }
            gamePlatformLabel.text = unwrappedGame.gamePlatform
            gamePublisherLabel.text = "Publisher: " + unwrappedGame.gamePublisher
            
        } else {
            NSLog("Crud... no model yet...")
            gameTitleLabel.text = "GameObject not loaded!"
        }
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
