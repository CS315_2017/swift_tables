//
//  FirstViewController.swift
//  Explore
//
//  Created by Rich Tanner on 10/18/17.
//  Copyright © 2017 Rich Tanner. All rights reserved.
//

import UIKit

class FirstViewController: UIViewController {
    
    @IBOutlet weak var aniView: UIView!
    @IBOutlet weak var hideyLabel: UILabel!
    
    var midFrame: CGRect!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
        
        aniView.backgroundColor = UIColor.red
        aniView.layer.cornerRadius = 15
        
        // set the frame to half the size of our containing view, and center it
        // we then save as our variable and can call it at any point during app lifecycle
        midFrame = CGRect(x: view.frame.width/4, y: view.frame.height/4, width: view.frame.width/2, height: view.frame.height/2)
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        
        UIView.animate(withDuration: 2, delay: 1, options: UIViewAnimationOptions.curveEaseIn, animations: { () -> Void in
            // cool stuff go here
            
            self.aniView.alpha = 0
            self.aniView.frame = self.midFrame
            self.hideyLabel.isHidden = true
            
            }) { (disbool: Bool) -> Void in
                // it's done.  Go do OTHER cool stuff
                UIView.animate(withDuration: 2, delay: 1, options: UIViewAnimationOptions.curveEaseIn, animations: { () -> Void in
                    // cool stuff go here
                    
                    self.aniView.alpha = 1
                    
                    }) { (disbool: Bool) -> Void in
                        // it's done.  Go do OTHER cool stuff
                        UIView.animate(withDuration: 2, delay: 1, options: UIViewAnimationOptions.curveEaseIn, animations: { () -> Void in
                            // cool stuff go here
                            
                            self.aniView.frame = self.view.frame
                            
                            }) { (disbool: Bool) -> Void in
                                // it's done.  Go do OTHER cool stuff
                                UIView.animate(withDuration: 2, delay: 1, options: UIViewAnimationOptions.curveEaseIn, animations: { () -> Void in
                                    // cool stuff go here
                                    
                                    self.aniView.frame = self.midFrame
                                    // we can make more than one adjustment in this block
                                    self.aniView.alpha = 0.0
                                    
                                    }) { (disbool: Bool) -> Void in
                                        // it's done.  Go do OTHER cool stuff
                                        UIView.animate(withDuration: 2, delay: 1, options: UIViewAnimationOptions.curveEaseIn, animations: { () -> Void in
                                            // cool stuff go here
                                            
                                            self.aniView.frame = self.midFrame
                                            // we can make more than one adjustment in this block
                                            self.aniView.alpha = 1
                                            
                                            }) { (disbool: Bool) -> Void in
                                                // it's done.  Go do OTHER cool stuff
                                                
                                                self.hideyLabel.isHidden = false
                                                
                                        }
                                        
                                }
                                
                        }
                        
                }
                
        }
        
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }


}

