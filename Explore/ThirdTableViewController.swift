//
//  ThirdTableViewController.swift
//  Explore
//
//  Created by Rich Tanner on 10/18/17.
//  Copyright © 2017 Rich Tanner. All rights reserved.
//

import UIKit

class ThirdTableViewController: UITableViewController {
    
    // MARK: - Local variable declarations
    
    var gamesList: Array<Array<GameObject>> = []
    var selectedGame: GameObject?
    var myDummy = DummyCreator()
    

    // MARK: - View Lifecycle Methods
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.tableView.backgroundColor = UIColor.lightGray
        
        // set gamesList to whatever we set up in DummyCreator instead
        gamesList = myDummy.makeGamesList()
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    // MARK: - Table view data source

    override func numberOfSections(in tableView: UITableView) -> Int {
        return gamesList.count
    }

    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        let thisList = gamesList[section]
        
        return thisList.count
    }
    
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "reuseIdentifier", for: indexPath as IndexPath)
        
        let thisSection = indexPath.section
        let thisRow = indexPath.row
        let arrayINeed = gamesList[thisSection]
        let itemINeed = arrayINeed[thisRow]

        // Configure the cell...
        cell.textLabel?.text = itemINeed.gameTitle
        cell.imageView?.image = UIImage(named: itemINeed.gameImageName)

        return cell
    }
    
    override func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
     
        var headerString = ""
        
        if section == 0 {
            headerString = "Old School Games"
        } else if section == 1 {
            headerString = "Games I Need to Play This Winter"
        } else {
            headerString = "Other Games (too many sections!)"
        }
        
        return headerString
    }
    
    override func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 75.0
    }
    
    override func tableView(_ tableView: UITableView, willSelectRowAt indexPath: IndexPath) -> IndexPath? {
        // set the needed GameObject here for later (not much later) use
        // We COULD do some cool stuff in here later...

        
    
        let thisSection = indexPath.section
    
        let thisRow = indexPath.row
        let arrayINeed = gamesList[thisSection]
        
        // set the needed GameObject here for later (not much later) use
        selectedGame = arrayINeed[thisRow]
        
        // return the thing that this function is actually FOR, I suppose...
        return indexPath
    }

    // MARK: - Navigation
    
    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
        
        // ohh... I don't like this.  But it works.
        // the "destinationViewController" is a UINavigationController, which we set up in the Storyboard.  We know this to be true... now.  But if that changes in the future, the code below BREAKS
        // but, this would still be considered "brittle" code, because we are basing these "assumptions" on our own Human Knowledge, not on what the code could infer for itself.
        
        
        let destinationNav = segue.destination as! UINavigationController
        let destinationDetailView = destinationNav.viewControllers[0] as! GameDetailsViewController
        
        destinationDetailView.thisGame = selectedGame
    }

}
